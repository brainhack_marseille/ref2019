
**Led by**: Your Name (email), ...

## Project Description
Main idea and context

## Skills required to participate
What different types of people could contribute?

## Links to resources
(Optional)
- [YourGitHubRepo](https://github.com/yourUserName/yourRepo)  
- [Article](https://link.to/the/article) 
- [Mattermost channel](https://link.to/the/mattermost/channel) 
- [Any other resource](https://link.to/an/other/resource)   

## Illustration
(Optional)  
[Link to a beautiful picture](url)
